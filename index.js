/**
 * For Boris Lykah
 * This is a conclusion for today's exercise in the office.
 * I was a little tired to be able to focus, but when I got home I was able to focus
 * Here is my solution to the string parse challenge. (Unique, no peaking or cheating!)
 * Enjoy!
 * @author Samvel Avanesov
 */

/**
 * Iterates over string with a pointer,
 * constructing arrays of values
 * @param {string} str
 * @return {number} i
 */
function captureArr(str, i) {
    const collection = [];
    const value = [];

    for (; i < str.length; i += 1) {
        const char = str[i].trim();
        const int = parseInt(char)
        const isNum = !isNaN(int);

        if (char === "]" || char === ",") {
            // [,] indicator of a value boundary
            // store new value in collection
            if (value.length > 0) {
                collection.push(Number(value.join("")))
                value.length = 0;
            }
            if (char === "]") {
                // array ended, break away from the loop to return new array
                break;
            }
        } else if (char === "[") {
            // beginning of an array, call recursive function to create nested/stacked arrays
            let ret = captureArr(str, i + 1)
            if (ret.collection.length > 0) {
                collection.push(ret.collection)
            }
            // assign new iterator value to ensure we continue through our string where the recurse has ended
            i = ret.i;
        } else if (isNum) {
            // simply save new int
            value.push(int)
        }
    }

    return {
        collection: collection || "",
        i
    }
}

/**
 * Calls recursive method to process string
 * @param {string} str 
 */
function parseStr(event) { 
    event.preventDefault();
    const str = event.target.elements.input.value;
    const stack = captureArr(str, 0).collection[0] || "";
    const stringified =  JSON.stringify(stack);
    event.target.elements.output.value = stringified;
    console.log("output", stringified, stack);
    return false;

}

// test
// console.log(JSON.stringify(parse("[123,[123,[123,123,123,[123],123],123,123,123],123]")));